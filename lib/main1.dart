import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: true,
    title: 'Carousel Pro',
    home: new CarouselPage(),
  ));
}

class CarouselPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Container(
          height: 1000.0,
          // width: 300.0,

          child: new Carousel(
            images: [
              // new NetworkImage(
              // 'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg'),
              // new NetworkImage(
              // 'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
              new ExactAssetImage("lib/assets/photo1.jpeg"),
              new ExactAssetImage("lib/assets/photo2.jpeg"),
              new ExactAssetImage("lib/assets/photo3.jpeg"),
              new ExactAssetImage("lib/assets/volunteers_mobile.png"),

              // new NetworkImage(
              // 'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
            ],
            // animationDuration: Duration(seconds: 1),

            autoplayDuration: Duration(seconds: 3),
          ),
        ),
      ),
      // floatingActionButton: Container(
      //   child: Text("data"),
      // ),
      
      floatingActionButton: 
      RaisedButton(
        
        child: Text("Skip"),
        onPressed: () {},      
        color: Colors.deepPurple,
        textColor: Colors.white,
      ),
      // persistentFooterButtons: <Widget>[
      //   new RaisedButton(

      //   )
      // ],
    );
  }
}
