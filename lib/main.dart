import 'package:intro_slider/intro_slider.dart';

import 'package:flutter/material.dart';
// import 'package:intro_slider/intro_slider.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MySplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MySplashScreen extends StatefulWidget {
  MySplashScreen({Key key}) : super(key: key);

  @override
  MySplashScreenState createState() => new MySplashScreenState();
}

// image carousel
class MySplashScreenState extends State<MySplashScreen> {
 List<Slide> slides = new List();

 @override
 void initState() {
   super.initState();

   slides.add(
     new Slide(
      //  title: "ERASER",
      //  description: "Allow miles wound place the leave had. To sitting subject no improve studied limited",
      //  pathImage: "lib/assets/volunteers_mobile.png",
      //  backgroundColor: Color(0xfff5a623),
      backgroundImage: "lib/assets/volunteers_mobile.png",
     ),
   );
   slides.add(
     new Slide(
      //  title: "PENCIL",
      //  description: "Ye indulgence unreserved connection alteration appearance",
      //  pathImage: "lib/assets/volunteers_mobile.png",
       backgroundColor: Color(0xff203152),
     ),
   );
   slides.add(
     new Slide(
      //  title: "RULER",
      //  description:
          //  "Much evil soon high in hope do view. Out may few northward believing attempted. Yet timed being songs marry one defer men our. Although finished blessing do of",
      //  pathImage: "lib/assets/volunteers_mobile.png",
       backgroundColor: Color(0xff9932CC),
     ),
   );
 }

 void onDonePress() {
   // TODO: go to next screen
 }

 void onSkipPress() {
   // TODO: go to next screen
 }

 @override
 Widget build(BuildContext context) {
   return new IntroSlider(
     slides: this.slides,
     onDonePress: this.onDonePress,
     onSkipPress: this.onSkipPress,
   );
 }
}